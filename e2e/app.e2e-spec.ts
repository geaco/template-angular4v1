import { TemplateAngular4Page } from './app.po';

describe('template-angular4 App', () => {
  let page: TemplateAngular4Page;

  beforeEach(() => {
    page = new TemplateAngular4Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
